using System.IO;
using System.Security.Cryptography.X509Certificates;
using System.Text.RegularExpressions;

namespace ZipCodeCheck{
    public class FileReader{
        public async void Reader(){
            string? activeUser = Environment.GetEnvironmentVariable("USER"); 
            string FilePath = $"/home/{activeUser}/ceps";

            List<string> ZipCodeList = new List<string>();

            if(FilePath is not null){
                ZipCodeList = File.ReadAllLines(FilePath).ToList();
                await ZipCode.ZipCodeApiAsync(ZipCodeList);   
            }

            else{
                Console.WriteLine("O documento está vazio!");
            }
        }
    }
}